﻿using System;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace LockWindows
{
    internal static class Program
    {
        /// <summary>
        /// 应用程序的主入口点。
        /// </summary>
        [STAThread]
        private static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            SuspendWinLogon();
            Application.Run(new Form1());
            ResumeWinLogon();
        }

        [DllImport("ntdll.dll")]
        public static extern int ZwSuspendProcess(IntPtr ProcessId);

        [DllImport("ntdll.dll")]
        public static extern int ZwResumeProcess(IntPtr ProcessId);

        private static void SuspendWinLogon()
        {
            Process[] pc = Process.GetProcessesByName("winlogon");
            if (pc.Length > 0)
            {
                ZwSuspendProcess(pc[0].Handle);
            }
        }

        private static void ResumeWinLogon()
        {
            Process[] pc = Process.GetProcessesByName("winlogon");
            if (pc.Length > 0)
            {
                ZwResumeProcess(pc[0].Handle);
            }
        }
    }
}