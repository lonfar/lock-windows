﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LockWindows
{
    public partial class Form2_test : Form
    {
        public Form2_test(bool aLock)
        {
            if (aLock)
            {
                //鎖屏+關屏
                LockWorkStation();
                SendMessage(this.Handle, (uint)0x0112, (IntPtr)0xF170, (IntPtr)2);
            }
            else
            {
                //禁止鼠標鍵盤動作+關屏
                BlockInput(true);
                System.Threading.Thread.Sleep(10);
                SendMessage(this.Handle, (uint)0x0112, (IntPtr)0xF170, (IntPtr)2);
                BlockInput(false);
            }
            this.Close();
            //Application.Exit();
            Environment.Exit(0);
        }
        //關屏
        [DllImport("user32.dll", CharSet = CharSet.Auto)]
        static extern IntPtr SendMessage(IntPtr hWnd, UInt32 Msg, IntPtr wParam, IntPtr lParam);
        //禁止鼠標鍵盤動作
        [return: MarshalAs(UnmanagedType.Bool)]
        [DllImport("user32.dll", CharSet = CharSet.Auto, ExactSpelling = true)]
        public static extern bool BlockInput([In, MarshalAs(UnmanagedType.Bool)] bool fBlockIt);
        //鎖屏
        [DllImport("user32.dll")]
        public static extern bool LockWorkStation();
    }
}
