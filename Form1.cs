﻿using System;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace LockWindows
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            this.ShowTime.Text = DateTime.Now.ToString("HH : mm : ss");
            this.ShowDate.Text = DateTime.Now.ToString("yyyy年MM月dd日 dddd");
            this.FormBorderStyle = FormBorderStyle.None;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            HookStart();
        }

        /// <summary>
        /// 防止屏闪
        /// </summary>
        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams cp = base.CreateParams;
                cp.ExStyle |= 0x02000000; // Turn on WS_EX_COMPOSITED
                return cp;
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            this.ShowTime.Text = DateTime.Now.ToString("HH : mm : ss");
            this.ShowDate.Text = DateTime.Now.ToString("yyyy年MM月dd日 dddd");
        }

        private void Form1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Modifiers.CompareTo(Keys.Control) == 0 && e.KeyCode == Keys.L)
            {
                label1.Visible = true;
                password.Text = "";
                password.Enabled = true;
                password.Visible = true;
                password.Focus();
            }
            if (e.KeyCode == Keys.Escape)
            {
                label1.Visible = false;
                password.Enabled = false;
                password.Visible = false;
            }
            if ((e.KeyCode == Keys.F4) && (e.Alt == true))  //屏蔽ALT+F4
            {
                e.Handled = true;
            }
        }

        private void password_KeyUp(object sender, KeyEventArgs e)
        {
            if (password.Text == "Aa12345")
            {
                Close();
                //Application.ExitThread();
            }
        }
        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            //取消关闭窗口
            e.Cancel = true;
            //卸载钩子
            HookClear();
            //最小化主窗口
            this.WindowState = FormWindowState.Minimized;
            //不在系统任务栏显示主窗口图标
            this.ShowInTaskbar = false;
            //提示气泡
            //notifyIcon1.ShowBalloonTip(2000, "最小化到托盘", "程序已经缩小到托盘，单击打开程序。", ToolTipIcon.Info);
        }

        private void notifyIcon1_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                if (this.WindowState == FormWindowState.Minimized)
                {
                    label1.Visible = false;
                    password.Text = "";
                    password.Enabled = false;
                    password.Visible = false;
                    //还原窗体
                    this.WindowState = FormWindowState.Maximized;
                    //安装钩子
                    HookStart();
                    //激活窗体并给予它焦点
                    this.Activate();
                }
                //激活窗体并获取焦点
                this.Activate();
            }
        }

        private void 显示ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (this.WindowState == FormWindowState.Minimized)
            {
                label1.Visible = false;
                password.Text = "";
                password.Enabled = false;
                password.Visible = false;
                //还原窗体
                this.WindowState = FormWindowState.Maximized;
                //安装钩子
                HookStart();
                //激活窗体并给予它焦点
                this.Activate();
            }
        }

        private void 退出ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //卸载钩子
            HookClear();
            this.Dispose();
            this.Close();
            Application.ExitThread();
        }

        #region 钩子函数

        public delegate int HookProc(int nCode, int wParam, IntPtr lParam);//定义全局钩子过程委托，以防被回收（钩子函数原型）

        private HookProc KeyBoardProcedure;

        //定义键盘钩子的相关内容，用于截获键盘消息
        private static int hHook = 0;//钩子函数的句柄

        public const int WH_KEYBOARD = 13;

        //钩子结构函数
        public struct KeyBoardHookStruct
        {
            public int vkCode;
            public int scanCode;
            public int flags;
            public int time;
            public int dwExtraInfo;
        }

        //安装键盘钩子
        public void HookStart()
        {
            if (hHook == 0)
            {
                //实例化一个HookProc对象
                KeyBoardProcedure = new HookProc(Form1.KeyBoardHookProc);

                //创建线程钩子
                hHook = Win32API.SetWindowsHookEx(WH_KEYBOARD, KeyBoardProcedure, Win32API.GetModuleHandle(Process.GetCurrentProcess().MainModule.ModuleName), 0);
                Win32API.ShowCursor(0);
                //如果设置线程钩子失败
                if (hHook == 0)
                {
                    HookClear();
                }
            }
        }

        //取消钩子
        public void HookClear()
        {
            bool rsetKeyboard = true;
            if (hHook != 0)
            {
                rsetKeyboard = Win32API.UnhookWindowsHookEx(hHook);
                Win32API.ShowCursor(1);
                hHook = 0;
            }
            if (!rsetKeyboard)
            {
                throw new Exception("取消钩子失败!");
            }
        }

        //对截获的键盘操作的处理
        public static int KeyBoardHookProc(int nCode, int wParam, IntPtr lParam)
        {
            if (nCode >= 0)
            {
                KeyBoardHookStruct kbh = (KeyBoardHookStruct)Marshal.PtrToStructure(lParam, typeof(KeyBoardHookStruct));

                if (kbh.vkCode == 91)//截获左边WIN键
                {
                    return 1;
                }
                if (kbh.vkCode == 92)//截获右边WIN键
                {
                    return 1;
                }
                if (kbh.vkCode == (int)Keys.Escape && (int)Control.ModifierKeys == (int)Keys.Control)//截获Ctrl+ESC键
                {
                    return 1;
                }
                if (kbh.vkCode == (int)Keys.Escape && (int)Control.ModifierKeys == (int)Keys.Alt)
                {
                    return 1;
                }
                if (kbh.vkCode == (int)Keys.F4 && (int)Control.ModifierKeys == (int)Keys.Alt)//截获ALT+F4
                {
                    return 1;
                }
                if (kbh.vkCode == (int)Keys.Tab && (int)Control.ModifierKeys == (int)Keys.Alt)//截获ALT+TAB
                {
                    return 1;
                }
                if (kbh.vkCode == (int)Keys.Delete && (int)Control.ModifierKeys == (int)Keys.Control + (int)Keys.Alt)
                {
                    return 1;
                }
                if (kbh.vkCode == (int)Keys.Escape && (int)Control.ModifierKeys == (int)Keys.Control + (int)Keys.Alt)   /* 截获Ctrl+Shift+Esc */
                {
                    return 1;
                }
            }

            return Win32API.CallNextHookEx(hHook, nCode, wParam, lParam);
        }

        #endregion


    }
}